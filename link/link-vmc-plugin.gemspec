# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "link-vmc-plugin/version"

Gem::Specification.new do |s|
  s.name        = "link-vmc-plugin"
  s.version     = VMCLink::VERSION.dup
  s.authors     = ["Dave Syer"]
  s.email       = ["dsyer@vmware.com"]
  s.homepage    = "http://cloudfoundry.com/"
  s.summary     = %q{
    
  }

  s.rubyforge_project = "link-vmc-plugin"

  s.files         = %w{Rakefile} + Dir.glob("lib/**/*")
  s.test_files    = Dir.glob("spec/**/*")
  s.require_paths = ["lib"]

  s.add_development_dependency "vmc", "~> 0.4.7"

  s.add_development_dependency "rspec"
  s.add_development_dependency "rake"

end
