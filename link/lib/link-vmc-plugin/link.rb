require "json"

class CFLink

  def initialize(client, link)
    @client = client
    @link = link
  end

  def create
    begin
      @client.base.request("GET", "/links/#{@link[:name]}")
      @client.base.request("PUT", "/links/#{@link[:name]}", :payload=>@link.to_json)
      # use NotFound (or APIError if the code is not a core one)?
    rescue CFoundry::AppNotFound
      @client.base.request("POST", "/links", :payload=>@link.to_json)
    end
  end

  def delete
    begin
      @client.base.request("DELETE", "/links/#{@link[:name]}")
    rescue CFoundry::AppNotFound
      # ignore
    end
  end

end

class CFLinks

  def initialize(client)
    @client = client
  end

  def links
    JSON.parse(@client.base.request("GET", '/links'))
  end

end
