require "vmc/plugin"
require "link-vmc-plugin/link"

module VMCLink
  class Link < VMC::CLI

    desc "Create or update a link to your cloud controller"
    group :admin
    input :url, :argument => :required,
      :desc => "Url to connect to"
    input :name, :argument => :required, 
      :desc => "Name of link"
    def link
      url = input[:url]
      name = input[:name]

      link = CFLink.new(client, input.inputs)

      with_progress("Creating link #{c(name, :name)}") do
        link.create
      end

    end

    desc "Delete a link"
    group :admin
    input :name, :argument => :required, 
      :desc => "Name of link"
    def unlink
      name = input[:name]

      link = CFLink.new(client, input.inputs)

      with_progress("Deleting link #{c(name, :name)}") do
        link.delete
      end

    end

    desc "List links to your cloud controller"
    group :admin
    def links
      link = CFLinks.new(client)

      res = with_progress("Getting links") do
        link.links
      end

      display_links_table(res)

    end

    def display_links_table(links)
      table(
        ["url", "name"],
        links.collect { |l|
          [ l['url'],
            c(l['name'], :name)
          ]
        })
    end

    filter(:start, :start_app) do |app|
      puts "Starting: #{app.name}"
      app
    end
  end
end
