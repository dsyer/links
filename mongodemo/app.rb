require 'sinatra'
require './database'

class Message
  include Database::Document
  attr_accessor :summary, :text
end

class App < Sinatra::Base
  configure do
    enable :inline_templates
  end
  get '/' do
    @title = "Messages : View All"
    @messages = Message.find().collect { |hash| Message.new(hash) }
    erb :list
  end
  get '/form' do
    @title = "Messages : Create"
    @message = Message.new
    erb :form
  end
  get '/:id' do
    @title = "Messages : View"
    @message = Message.new(Message.get(params[:id]))
    erb :view
  end
  post '/' do
    Message.new(params).save()
    redirect to '/'
  end
end

__END__
@@layout
<!DOCTYPE html>
<html>
  <head>
    <title><%= @title || "Messages" %></title>
    <link rel="stylesheet"
      href="/css/bootstrap.min.css"/>
  </head>
  <body>
    <div class="container">
      <h1><%= @title || "Messages" %></h1>
      <%= yield %>
    </div>
  </body>
</html>
@@list
<!DOCTYPE html>
<div class="pull-right">
  <a href="./form">Create Message</a>
</div>
<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <td>ID</td>
      <td>Created</td>
      <td>Summary</td>
    </tr>
  </thead>
  <tbody>
    <% unless @messages and !@messages.empty? %>
    <tr>
      <td colspan="3">No messages</td>
    </tr>
    <% else @messages.each do |message| %>
    <tr>
      <td><%=message.id%></td>
      <td><%=message.created%></td>
      <td><a href="./<%=message.id%>"><%=message.summary%></a></td>
    </tr>
   <% end end %>
   </tbody>
 </table>
@@form
<% if @formErrors %>
  <div class="alert alert-error">
    <%= @formErrors %>
  </div>
<% end %>
<form id="messageForm" action="./" method="post">
  <div class="pull-right"><a href="./">Messages</a></div>
  <label for="summary">Summary</label>
  <input type="text"
    name="summary"
    id="summary"
    value="<%= @message.summary %>"/>
  <label for="text">Message</label>
  <textarea
    name="text"
    id="text"><%= @message.text %></textarea>
  <div class="form-actions">
    <input type="submit" value="Create"/>
  </div>
</form>
@@view
<% if @globalMessage %>
  <div class="alert alert-success">
    <%= @globalMessage %>
  </div>
<% end %>
<div class="pull-right">
  <a href="./">Messages</a>
</div>
<dl>
  <dt>ID</dt>
  <dd id="id"><%=@message.id%></dd>
  <dt>Date</dt>
  <dd id="created"><%=@message.created%></dd>
  <dt>Summary</dt>
  <dd id="summary"><%=@message.summary%></dd>
  <dt>Message</dt>
  <dd id="text"><%=@message.text%></dd>
</dl>
