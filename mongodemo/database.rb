require 'mongo'
require 'json'

module Database

  module Document

    @@collection = nil

    attr_accessor :created
    attr_reader :id

    def initialize(hash = nil)
      @created = Time.now
      unless hash == nil
        hash.each do |k,v|
          k = 'id' if k == '_id'
          self.instance_variable_set("@#{k}", v)
          self.class.send(:define_method, k, proc{self.instance_variable_get("@#{k}")})
          self.class.send(:define_method, "#{k}=", proc{|v| self.instance_variable_set("@#{k}", v)})
        end
      end
    end

    def save()
      Database::db[self.class.collection].save(self.to_hash)
    end

    def to_hash
      hash = {}
      instance_variables.each {|var| hash[var.to_s.delete("@")] = instance_variable_get(var) }
      hash
    end

    def self.included base
      object_name = base.name.split("::").last
        .gsub(/([a-z])([A-Z])/,'\1_\2').downcase
      base.extend ClassMethods
      base.instance_eval do
        self.collection = object_name.end_with?('s') ? "#{object_name}es" : "#{object_name}s"
      end
    end

    module ClassMethods
      attr_accessor :collection
      def find(opts={})
        Database::db[collection].find(opts)      
      end
      def get(id)
        Database::db[collection].find_one(:_id=>BSON::ObjectId(id))
      end
    end

  end

  @@db = nil

  def self.db
    unless @@db 
      # MONGODB_URI=mongodb://<user>:<password>@<host>/messagedb
      dbname = ENV['MONGODB_URI'] ? URI(ENV['MONGODB_URI']).path[1..-1] : 'test'
      puts "Connecting to: #{ENV['MONGODB_URI']||'local mongo'} with db=#{dbname}"
      connection = Mongo::Connection.new
      @@db = connection.db(dbname)
    end
    @@db
  end

end
