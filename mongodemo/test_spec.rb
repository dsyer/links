require "./app"
require 'rack/test'

describe App do

  include Rack::Test::Methods
  
  def app
    App.new
  end

  it "should be testable" do
    get '/'
    last_response.should be_ok
  end

end
