require 'sinatra'
require './filter'

class App < Sinatra::Base
  get '/' do
    content_type :json
    {:message=>"hello"}.to_json
  end
end

use Filter

run App
