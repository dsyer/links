require 'sinatra'
require 'json'

class Filter < Sinatra::Base
  set :show_exceptions, false
  def call(env)
    status, headers, body = super
    if status < 300 and status >= 200
      body = body.join('') if body.is_a? Array
      result = JSON.parse(body) 
      add_meta!(result)
      body = [result.to_json]
      headers.delete("Content-Length")
    end
    [status, headers, body]
  end
  private
  def add_meta!(result)
    result.each { |it| add_meta(it) } if result.is_a? Array
    if result.is_a? Hash
      meta = (result[:meta] ||= {})
      meta[:enhanced] = true
      meta[:timestamp] ||= Time.now
    end
  end
end
