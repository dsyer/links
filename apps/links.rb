require 'sinatra'
require 'json'
require './common'

class Links < Sinatra::Base

  include VCAP::Resource::Helper

  Sinatra::Delegator::delegate :links

  set :raise_errors, false
  set :show_exceptions, false
  set :request_method do |*values|
    condition { values.include? request.request_method }
  end

  def links
    Common.links
  end

  before :request_method => ['POST', 'PUT', 'DELETE'] do
    # N.B. the real cloud controller doesn't do this!
    # check_scope(request.env, ["cloud_controller.write"])
  end

  def not_found_error
    {:code=>301, :description=>"Link not found"}    
  end

  get '/' do
    links.values.to_json
  end

  get '/:id' do |id|
    unless links[id]
      status 404
      return not_found_error.to_json
    end
    links[id].to_json
  end

  delete '/:id' do |id|
    unless links[id]
      status 404
      return not_found_error.to_json
    end
    links.delete(id)
    nil
  end

  put '/:id' do |id|
    unless links[id]
      status 404
      return not_found_error.to_json
    end
    link = links[id]
    input = VCAP.symbolize_keys(JSON.parse(request.body.read))
    link.merge!(input)
    nil
  end

  post '/' do
    link = VCAP.symbolize_keys(JSON.parse(request.body.read))
    id = link[:name]
    links[id] = link
    location = "#{request.scheme}://#{request.host}:#{request.port}#{request.path}#{id}"
    headers "Location" => location
    status 302
    {:result => 'success', :redirect => location }.to_json
  end

end

class Services < Sinatra::Base
  include Common
  def not_found_error
    {:code=>500, :description=>"Service not found"}    
  end
  get '/' do
    compose_all(request.path,request.env['HTTP_AUTHORIZATION']).to_json
  end
  get '/:name' do
    res = compose_one(request.path,request.env['HTTP_AUTHORIZATION'])
    unless res
      status 404
      return not_found_error.to_json
    end
    res.to_json
  end
  get '/v1/offerings' do
    {}.to_json
  end
 end
