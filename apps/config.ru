#\--port 9293

require 'vcap/resource'
require './apps'
require './links'

config = VCAP::Resource::Bootstrap.new(:name=>'sample', :config=>'config.yml').config
puts "#{config}"
use VCAP::Resource::Base, config

map '/apps' do
  Apps.set :config, config
  run Apps
end

map '/links' do
  Links.set :config, config
  run Links
end

map '/services' do
  Services.set :config, config
  run Services
end

class Resources < Sinatra::Base
  post '/' do
    nil # we know about no files
  end
end

map '/resources' do
  run Resources
end
