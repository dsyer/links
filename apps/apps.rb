require 'sinatra'
require 'json'
require './common'

class Apps < Sinatra::Base

  include VCAP::Resource::Helper
  include Common

  Sinatra::Delegator::delegate :apps, :remotes

  set :raise_errors, false
  set :show_exceptions, false
  set :request_method do |*values|
    condition { values.include? request.request_method }
  end

  def apps
    @@apps ||= {}
  end

  def remotes
    @@remotes ||= {}
  end

  before :request_method => ['POST', 'PUT', 'DELETE'] do
    # N.B. the real cloud controller doesn't do this!
    # check_scope(request.env, ["cloud_controller.write"])
  end

  def not_found_error
    {:code=>301, :description=>"Application not found"}    
  end

  get '/' do
    res = apps.values
    res += compose_all(request.path,request.env['HTTP_AUTHORIZATION'])
    res.to_json
  end

  get '/:id' do |id|
    app = apps[id] || compose_one(request.path,request.env['HTTP_AUTHORIZATION'])
    unless app
      remotes.delete(id) if remotes[id]
      status 404
      return not_found_error.to_json
    end
    merge_remotes(app)
    app.to_json
  end

  delete '/:id' do |id|
    unless apps[id]
      status 404
      return not_found_error.to_json
    end
    apps.delete(id)
    nil
  end

  put '/:id' do |id|
    app = apps[id]
    unless app
      app = compose_one(request.path,request.env['HTTP_AUTHORIZATION'])
    end
    unless app
      status 404
      return not_found_error.to_json
    end
    puts "Existing app: #{app}"
    input = VCAP.symbolize_keys(JSON.parse(request.body.read))
    puts "Updates: #{input}"
    unless app[:link]
      app[:runningInstances] = input[:instances] || app[:instances] || 1
      app.overlay!(input)
    else
      input[:updated_services] = (input[:services]||[]) - (app['services']||[]) # TODO why not symbolized key?
      input[:link] = app[:link]
      update_remotes(input, app)
    end
    nil
  end

  post '/' do
    app = VCAP.symbolize_keys(JSON.parse(request.body.read))
    id = app[:name]
    app.merge!({:meta => {:debug=>nil,:console=>false,:version=>1,:created=>Time.now.to_i},
      :uris => [], :services => [], :env => []})
    apps[id] = app
    location = "#{request.scheme}://#{request.host}:#{request.port}#{request.path}#{id}"
    headers "Location" => location
    status 302
    {:result => 'success', :redirect => location }.to_json
  end

  post '/:id/application' do |id|
    app = apps[id]
    if app
      unless params['application'] and (file = params['application'][:tempfile])
        return nil
      end
      logger.debug "Uploading: #{params['application']}"
      # TODO: Sinatra wraps this somehow so files in /tmp get deleted at the end of the block
      File.open("/tmp/#{params['application'][:filename]}", 'wb') {|f| f.write file.read }
      nil
    else # remote app
      token = request.env['HTTP_AUTHORIZATION']
      app = compose_one("/apps/#{id}",token)
      return unless app && (link = app[:link])
      logger.debug "Uploading: #{params['application']}"
      request_headers = {:authorization=>"#{token}", :accept=>:json, :content_type=>request.env["CONTENT_TYPE"]}
      RestClient.post("#{link[:url]}#{request.path}", request.body.read, request_headers)
    end
  end

  def merge_remotes(app)
    id = app['name'] || app[:name]
    envkey = app['env'] ? 'env' : :env
    serviceskey = app['services'] ? 'services' : :services
    if remotes[id] && app[envkey]
      remotes[id].each do |name,env|
        env.each do |item|
          app[envkey] << item unless app[envkey].include?(item)
        end
        app[serviceskey] << name unless app[serviceskey].include?(name)
      end
    end
  end

  def update_remotes(app, old)

    services = app[:services]
    return if !services

    puts "Updating: #{app}"
    app[:updated_services] ||= []
    app[:removed_services] ||= []
    id = app[:name]
    link = app[:link]

    if remotes[id]
      removed = app[:removed_services]
      remotes[id].each do |name,_|
        removed << name unless removed.include?(name) || services.include?(name)
      end
      puts "Removing services: [#{removed.join(',')}]"
    end
    token = request.env['HTTP_AUTHORIZATION']
    remote_services = {}

    with_links('/services', token) do |values,link|
      values.each do |remote|
        name = remote['name']
        remote['link'] = link
        remote_services[name] = remote
      end
    end

    updates = app.dup
    oldenv = app[:env].dup

    unless (common = app[:updated_services]).empty?
      common.each do |name|
        remote = remote_services[name]
        if remote && remote['link']!=link
          puts "Adding service: #{remote}"
          if (env = remote['env'])
            remotes[id] ||= {}
            remotes[id][name] = env
            env.each do |value|
              updates[:env] << value unless updates[:env].include?(value)
            end
          end
          # The remote app does not have this service
          updates[:services].delete(name)
        end
      end
    end

    unless (common = app[:removed_services]).empty?
      common.each do |name|
        remote = remote_services[name]
        if remote
          puts "Removing service: #{remote}"
          if (env = remote['env'].dup)
            remotes[id] ||= {}
            remotes[id].delete(name)
            env.each do |value|
              updates[:env].delete(value)
            end
          end
        end
        updates[:services].delete(name)
      end
    end

    updates.delete(:removed_services)
    updates.delete(:updated_services)
    updates.delete(:link)

    if link

      puts "Found link: #{link}"
      puts "Remotes: #{remotes}"
      updates.delete(:link)
      puts "Updated: #{updates}"

      need_stop |= updates != old

      if need_stop
        puts "Stopping #{link[:url]}: #{updates}"
        updates[:state] = "STOPPED"
        begin
          RestClient.put("#{link[:url]}/apps/#{id}", updates.to_json, :authorization=>"#{token}")
        rescue => e
          puts "Failed :-( #{e}"
          # ignore
        end
        updates[:meta][:version] += 1
      end

      need_start = app[:state]=="STARTED" && (need_stop || old[:state]!="STARTED")

      if need_start
        updates[:state] = "STARTED"
        puts "Starting #{link[:url]}: #{updates}"
        begin
          RestClient.put("#{link[:url]}/apps/#{id}", updates.to_json, :authorization=>"#{token}")
        rescue => e
          puts "Failed :-( #{e}"
          # ignore
        end
      end

    end

    merge_remotes(app)

  end

end
