require 'restclient'
require 'json'

class Hash
  def overlay!(input)
    merge!(input) do |k,o,n|
      if o.is_a?(Hash)
        o.merge!(o)
      else
        o = n
      end
      o
    end
  end
end

module Common
  def Common.links
    @@links ||= {}
  end
  def with_links(path, token, &blk)
    Common.links.each do |_,link|
      begin
        res = JSON.parse(RestClient.get("#{link[:url]}#{path}", :authorization=>"#{token}"), :symbolize_keys=>true)
        if res.is_a? Array
          res.each { |item| item[:link] = link if item.is_a? Hash }
        else
          res[:link] = link
        end
        return unless blk.call(res,link)
      rescue
        # ignore
      end
    end
  end
  def compose_all(path, token)
    res = []
    with_links(path,token) { |it| res += it; true }
    res    
  end
  def compose_one(path, token)
    res = nil
    with_links(path,token) { |it| res = it; false }
    res
  end
end
