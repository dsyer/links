require 'sinatra'
require 'json'
class Services < Sinatra::Base
  def not_found_error
    {:code=>500, :description=>"Service not found"}    
  end
  get '/' do
    [].to_json
  end
  get '/:name' do
    status 404
    return not_found_error.to_json
  end
  get '/v1/offerings' do
    {}.to_json
  end
end
