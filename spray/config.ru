#\--port 9294

require 'vcap/resource'
require './apps'
require './services'

config = VCAP::Resource::Bootstrap.new(:name=>'sample', :config=>'config.yml').config
puts "#{config}"
use VCAP::Resource::Base, config

map '/apps' do
  Apps.set :config, config
  run Apps
end

map '/services' do
  Services.set :config, config
  run Services
end

class Resources < Sinatra::Base
  post '/' do
    nil # we know about no files
  end
end

map '/resources' do
  run Resources
end
