require 'sinatra'
require 'json'
require 'fileutils'

class Apps < Sinatra::Base

  include VCAP::Resource::Helper

  Sinatra::Delegator::delegate :apps, :remotes

  set :raise_errors, false
  set :show_exceptions, false
  set :request_method do |*values|
    condition { values.include? request.request_method }
  end

  def apps
    @@apps ||= {}
  end

  def remotes
    @@remotes ||= {}
  end

  before :request_method => ['POST', 'PUT', 'DELETE'] do
    # N.B. the real cloud controller doesn't do this!
    # check_scope(request.env, ["cloud_controller.write"])
  end

  def not_found_error
    {:code=>301, :description=>"Application not found"}    
  end

  get '/' do
    res = apps.values
    res.to_json
  end

  get '/:id' do |id|
    app = apps[id]
    unless app
      remotes.delete(id) if remotes[id]
      status 404
      return not_found_error.to_json
    end
    app.to_json
  end

  delete '/:id' do |id|
    app = apps[id]
    unless app
      status 404
      return not_found_error.to_json
    end
    spawn "kill -9 #{app[:pid]}" if app[:pid]
    apps.delete(id)
    nil
  end

  put '/:id' do |id|
    app = apps[id]
    unless app
      status 404
      return not_found_error.to_json
    end
    puts "Existing app: #{app}"
    input = VCAP.symbolize_keys(JSON.parse(request.body.read))
    puts "Updates: #{input}"
    target_state = input[:state]
    existing_state = app[:state]
    unless target_state == existing_state
      stop app if target_state == "STOPPED"
      start app if target_state == "STARTED"
    end
    app.merge!(input)
    nil
  end

  post '/' do
    app = VCAP.symbolize_keys(JSON.parse(request.body.read))
    id = app[:name]
    app.merge!({:meta => {:debug=>nil,:console=>false,:version=>1,:created=>Time.now.to_i},
      :uris => [], :services => [], :env => []})
    apps[id] = app
    location = "#{request.scheme}://#{request.host}:#{request.port}#{request.path}#{id}"
    headers "Location" => location
    status 302
    {:result => 'success', :redirect => location }.to_json
  end

  post '/:id/application' do |id|
    app = apps[id]
    unless app
      status 404
      return not_found_error.to_json
    end
    if app
      unless params['application'] and (file = params['application'][:tempfile])
        return nil
      end
      # Sinatra wraps this somehow so files in /tmp get deleted at the end of the block
      FileUtils.mkdir_p("./tmp/#{id}")
      Dir.chdir("./tmp/#{id}") do
        `rm -rf *`
        File.open("../#{params['application'][:filename]}", 'wb') {|f| f.write file.read }
        `unzip ../#{params['application'][:filename]}`
        app[:dir] = "./tmp/#{id}"
      end
      start app
      nil
    end
  end

  def extract_port(uris)
    return nil unless uris and !uris.empty?
    uri = uris.find { |uri| uri.match("localhost:[0-9]*") }
    uri[/:([0-9]*)/,1] if uri
  end

  def grab_ephemeral_port
    socket = TCPServer.new("0.0.0.0", 0)
    socket.setsockopt(Socket::SOL_SOCKET, Socket::SO_REUSEADDR, true)
    Socket.do_not_reverse_lookup = true
    socket.addr[1]
  ensure
    socket.close
  end

  def stop(app)
    %x(kill -9 #{app[:pid]}) if app[:state]=="STARTED" and app[:pid]
    puts "Stopped: #{app}"
    app.delete(:pid)
    app[:runningInstances] = 0
  end

  def start(app)
    stop app if app[:pid]
    Dir.chdir(app[:dir]) do
      port = app[:port] || extract_port(app[:uris]) || grab_ephemeral_port
      app[:port] = port
      pid = spawn("bash", "-c", ". ~/.rvm/scripts/rvm && rvm use 1.9.2 && bundle && exec bundle exec rackup --port #{port}")
      Process.detach(pid)
      app[:pid] = pid
      app[:state] = "STARTED"
      app[:uris] = ["localhost:#{port}"]
      app[:runningInstances] = app[:instances] || 1
      puts "Started: #{app}"
    end
  end

end
