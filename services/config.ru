require 'vcap/resource'
require './services'

config = VCAP::Resource::Bootstrap.new(:name=>'sample', :config=>'config.yml').config
puts "#{config}"
use VCAP::Resource::Base, config

class Apps < Sinatra::Base
  get '/' do
    [].to_json
  end
end

map '/apps' do
  run Apps
end

map '/services' do
  Services.set :config, config
  run Services
end

class Resources < Sinatra::Base
  post '/' do
    request.body # we know about all files
  end
end

map '/resources' do
  run Resources
end
