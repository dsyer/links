require 'sinatra'
require 'json'

class Services < Sinatra::Base

  include VCAP::Resource::Helper

  Sinatra::Delegator::delegate :services

  set :raise_errors, false
  set :show_exceptions, false
  set :request_method do |*values|
    condition { values.include? request.request_method }
  end

  def services
    @@services ||= {}
  end

  before :request_method => ['POST', 'PUT', 'DELETE'] do
    # N.B. the real cloud controller doesn't do this!
    # check_scope(request.env, ["cloud_controller.write"])
  end

  def not_found_error
    {:code=>301, :description=>"Service not found"}    
  end

  get '/' do
    services.values.to_json
  end

  get '/v1/offerings' do
    settings.config[:services].to_json
  end

  get '/:id' do |id|
    unless services[id]
      status 404
      return not_found_error.to_json
    end
    services[id].to_json
  end

  delete '/:id' do |id|
    unless services[id]
      status 404
      return not_found_error.to_json
    end
    services.delete(id)
    nil
  end

  put '/:id' do |id|
    unless services[id]
      status 404
      return not_found_error.to_json
    end
    service = services[id]
    input = VCAP.symbolize_keys(JSON.parse(request.body.read))
    nil
  end

  post '/' do
    service = VCAP.symbolize_keys(JSON.parse(request.body.read))
    id = service[:name]
    # TODO: service[:name] is private to the user, should not be in config
    service[:env] = settings.config[:credentials][id.to_sym] || {}
    services[id] = service
    puts "Provisioning: env:#{service[:env]} for service #{id}"
    location = "#{request.scheme}://#{request.host}:#{request.port}#{request.path}#{id}"
    headers "Location" => location
    status 302
    {:result => 'success', :redirect => location }.to_json
  end

end
