require 'sinatra'
require 'json'
require './common'

class Info < Sinatra::Base

  include VCAP::Resource::Helper
  include Common

  set :raise_errors, false
  set :show_exceptions, false

  get '/' do
    proxy(:get)
  end

  get '/runtimes' do
    proxy(:get)
  end

end

class Apps < Sinatra::Base

  include VCAP::Resource::Helper
  include Common

  set :raise_errors, false
  set :show_exceptions, false

  def not_found_error
    {:code=>301, :description=>"Application not found"}    
  end

  get '/' do
    proxy(:get)
  end

  get '/:id' do |id|
    proxy(:get)
  end

  delete '/:id' do |id|
    proxy(:delete)
  end

  put '/:id' do |id|
    proxy(:put, request.body.read)
  end

  post '/' do
    proxy(:post, request.body.read)
  end

  post '/:id/application' do
    if params['application'] and (file = params['application'][:tempfile])
      logger.debug "Uploading: #{params['application']}"
    end
    proxy(:post, request.body.binread)
  end

end

class Resources < Sinatra::Base

  include VCAP::Resource::Helper
  include Common

  set :raise_errors, false
  set :show_exceptions, false

  post '/' do
    proxy(:post, request.body.read)
  end

end

