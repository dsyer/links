require 'restclient'

module Common
  def remote
    @@remote ||= 'api.cloudfoundry.com'
  end
  def token
    request.env['HTTP_AUTHORIZATION']
  end
  def proxy(method, body = nil, request_headers = {})
    request_headers = request_headers.dup.merge!(:authorization=>"#{token}")
    begin
      case method
      when :get
        status, body = [200, RestClient.get("#{remote}/#{request.path}", request_headers)]
      when :delete
       status, body = [200, RestClient.delete("#{remote}/#{request.path}", request_headers)]
      when :post
        status, body = RestClient.post("#{remote}/#{request.path}", body, request_headers) { |response, request, result, &block|
          if response.code == 302
            location = response.headers[:location]
            headers "Location" => location
            [response.code, {:result => 'success', :redirect => location}.to_json]
          else
            [response.code, response.body]
          end
        }
      when :put
        status, body = [200, RestClient.put("#{remote}/#{request.path}", body, request_headers)]
      end
    rescue RestClient::Exception => e
      status, body = [e.http_code, e.response]
    end
    status status
    body
  end
end
