require 'sinatra'
require 'json'
require './common'

class Services < Sinatra::Base

  include VCAP::Resource::Helper
  include Common

  set :raise_errors, false
  set :show_exceptions, false
  set :request_method do |*values|
    condition { values.include? request.request_method }
  end

  before :request_method => ['POST', 'PUT', 'DELETE'] do
    # N.B. the real cloud controller doesn't do this!
    # check_scope(request.env, ["cloud_controller.write"])
  end

  def not_found_error
    {:code=>301, :description=>"Service not found"}    
  end

  get '/' do
    proxy(:get)
   end

  get '/v1/offerings' do
    proxy(:get)
   end

  get '/:id' do |id|
    proxy(:get)
   end

  delete '/:id' do |id|
    proxy(:delete)
  end

  put '/:id' do |id|
    proxy(:put, request.body.read)
  end

  post '/' do
    proxy(:post, request.body.read)
  end

end
