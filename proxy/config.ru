require 'vcap/resource'
require './apps'
require './services'

config = VCAP::Resource::Bootstrap.new(:name=>'sample', :config=>'config.yml').config
puts "Config: #{config}"
use VCAP::Resource::Base, config

map '/apps' do
  Apps.set :config, config
  run Apps
end

map '/info' do
  Info.set :config, config
  run Info
end

map '/services' do
  Services.set :config, config
  run Services
end

map '/resources' do
  run Resources
end
