`rm -rf ./tmp`
app = {}
id="foo"
params={"application"=>{:filename=>"foo.zip",:tempfile=>"/tmp/foo.zip"}}
FileUtils.mkdir_p("./tmp/#{id}")
file = File.new(params['application'][:tempfile])
Dir.chdir("./tmp/#{id}") do
  File.open("../#{params['application'][:filename]}", 'wb') {|f| f.write file.read }
  pid = spawn("bash", "-c", ". ~/.rvm/scripts/rvm; unzip ../#{params['application'][:filename]}; rvm use 1.9.2; bundle; bundle exec rackup")
  app[:pid] = pid
end
puts app
