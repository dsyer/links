require 'sinatra'
require 'json'

class App < Sinatra::Base
  get '/' do
    content_type :json
    {:message=>"hello"}.to_json
  end
end

run App
